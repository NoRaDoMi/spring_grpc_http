package com.noradomi.spring.grpc.core.controller;

import com.noradomi.spring.grpc.core.model.CoreServiceGrpc;
import com.noradomi.spring.grpc.core.model.PingRequest;
import com.noradomi.spring.grpc.core.model.PingResponse;
import com.noradomi.spring.grpc.core.service.PingService;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

/** Created by phucvt Date: 06/10/2021 */
@Slf4j
@GRpcService
public class GrpcController extends CoreServiceGrpc.CoreServiceImplBase {
  @Autowired PingService pingService;

  @Override
  public void ping(PingRequest request, StreamObserver<PingResponse> responseObserver) {
    responseObserver.onNext(pingService.ping(request));
    responseObserver.onCompleted();
  }
}
